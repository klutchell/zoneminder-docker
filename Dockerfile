FROM ubuntu:bionic

ENV DEBCONF_NONINTERACTIVE_SEEN true
ENV DEBIAN_FRONTEND noninteractive

RUN apt-get update \
    && apt-get install --no-install-recommends -y curl gnupg \
    && apt-key adv --keyserver keyserver.ubuntu.com --recv-keys ABE4C7F993453843F0AEB8154D0BF748776FFB04 \
    && echo deb http://ppa.launchpad.net/iconnor/zoneminder-1.34/ubuntu bionic main > /etc/apt/sources.list.d/zoneminder.list \
    && apt-get update \
    && apt-get install -y zoneminder \
    && a2enconf zoneminder \
    && a2enmod rewrite cgi \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/*

VOLUME /var/cache/zoneminder/events /var/cache/zoneminder/images /var/lib/mysql /var/log/zm

EXPOSE 80

COPY entrypoint.sh /

ENTRYPOINT [ "/entrypoint.sh" ]

ARG BUILD_DATE
ARG BUILD_VERSION
ARG VCS_REF

LABEL org.opencontainers.image.authors "Kyle Harding <https://klutchell.dev>"
LABEL org.opencontainers.image.url "https://gitlab.com/klutchell/zoneminder-docker"
LABEL org.opencontainers.image.documentation "https://gitlab.com/klutchell/zoneminder-docker"
LABEL org.opencontainers.image.source "https://gitlab.com/klutchell/zoneminder-docker"
LABEL org.opencontainers.image.title "klutchell/zoneminder"
LABEL org.opencontainers.image.description "Zoneminder is a full-featured, open source, state-of-the-art video surveillance software system"
LABEL org.opencontainers.image.created "${BUILD_DATE}"
LABEL org.opencontainers.image.version "${BUILD_VERSION}"
LABEL org.opencontainers.image.revision "${VCS_REF}"
