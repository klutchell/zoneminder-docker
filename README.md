# unofficial zoneminder multiarch docker image

[![Gitlab pipeline status](https://img.shields.io/gitlab/pipeline/klutchell/zoneminder-docker?style=flat-square)](https://gitlab.com/klutchell/zoneminder-docker/pipelines)
[![Docker Pulls](https://img.shields.io/docker/pulls/klutchell/zoneminder.svg?style=flat-square)](https://hub.docker.com/r/klutchell/zoneminder/)
[![Docker Stars](https://img.shields.io/docker/stars/klutchell/zoneminder.svg?style=flat-square)](https://hub.docker.com/r/klutchell/zoneminder/)

[Zoneminder](https://www.zoneminder.com/) is a full-featured, open source, state-of-the-art video surveillance software system.

## Architectures

The architectures supported by this image are:

- `linux/amd64`
- `linux/arm64`
- `linux/arm/v7`

Simply pulling `klutchell/zoneminder` should retrieve the correct image for your arch.

## Build

```bash
# build a local image
docker build . -t klutchell/zoneminder

# cross-build for another platform (eg. arm32v6)
export DOCKER_CLI_EXPERIMENTAL=enabled
docker run --rm --privileged multiarch/qemu-user-static --reset -p yes
docker buildx create --use --driver docker-container
docker buildx build . --platform linux/arm/v6 --load -t klutchell/zoneminder
```

## Test

```bash
# run image in detached mode and check the logs for successful startup
docker run -d --rm --name zoneminder klutchell/zoneminder
docker logs -f zoneminder
docker stop zoneminder
```

## Usage

ZoneMinder Documentation: <https://zoneminder.readthedocs.io/en/latest/index.html>

```bash
# run with internal database
docker run -d -t -p 8080:80 \
    -e TZ='America/Toronto' \
    -v ~/zoneminder/events:/var/cache/zoneminder/events \
    -v ~/zoneminder/images:/var/cache/zoneminder/images \
    -v ~/zoneminder/mysql:/var/lib/mysql \
    -v ~/zoneminder/logs:/var/log/zm \
    --shm-size="512m" \
    --name zoneminder \
    klutchell/zoneminder

# run with external database
docker run -d -t -p 8080:80 \
    -e TZ='America/Toronto' \
    -e ZM_DB_USER='zmuser' \
    -e ZM_DB_PASS='zmpassword' \
    -e ZM_DB_NAME='zoneminder_database' \
    -e ZM_DB_HOST='my_central_db_server' \
    -v ~/zoneminder/events:/var/cache/zoneminder/events \
    -v ~/zoneminder/images:/var/cache/zoneminder/images \
    -v ~/zoneminder/logs:/var/log/zm \
    --shm-size="512m" \
    --name zoneminder \
    klutchell/zoneminder
```

## Author

Kyle Harding <https://klutchell.dev>

[Buy me a beer](https://kyles-tip-jar.myshopify.com/cart/31356319498262:1?channel=buy_button)

[Buy me a craft beer](https://kyles-tip-jar.myshopify.com/cart/31356317859862:1?channel=buy_button)

## Contributing

Please open an issue or submit a pull request with any features, fixes, or changes.

<https://gitlab.com/klutchell/zoneminder-docker/issues>

## Acknowledgments

- <https://www.zoneminder.com/>
- <https://github.com/ZoneMinder/zmdockerfiles>
- <https://github.com/dlandon/zoneminder>

## Licenses

- klutchell/zoneminder: [MIT License](https://gitlab.com/klutchell/zoneminder-docker/blob/master/LICENSE)
- zoneminder/zoneminder: [GNU General Public License v2.0](https://github.com/ZoneMinder/zoneminder/blob/master/LICENSE)
